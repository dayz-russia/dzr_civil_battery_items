class dzr_SmallFlashlightLight extends SpotLightBase
{
	private static float m_DefaultRadius = 26; // 30 default
	private static float m_DefaultBrightness = 0.7; // 6 default
	private static float m_DefaultAngle = 54; // 120 default

	void dzr_SmallFlashlightLight()
	{
		SetVisibleDuringDaylight(true);
		SetRadiusTo(m_DefaultRadius);
		SetSpotLightAngle(m_DefaultAngle);
		SetCastShadow(true);
		EnableSpecular(true);
		SetBrightnessTo(m_DefaultBrightness);
		SetFadeOutTime(0.2);
		SetAmbientColor(0.55, 0.75, 1.0); //(0.75, 0.95, 1.0);
		SetDiffuseColor(0.55, 0.75, 1.0);
	}
	
	void SetIntensity( float coef, float time )
	{
		FadeBrightnessTo(m_DefaultBrightness * coef, time);
	}
	
}
class dzr_SmallFlashlight extends ItemBase
{
	dzr_SmallFlashlightLight m_Light;
	
	override void OnWorkStart()
	{
		if ( !GetGame().IsServer()  ||  !GetGame().IsMultiplayer() ) // Client side
		{
			m_Light = dzr_SmallFlashlightLight.Cast(  ScriptedLightBase.CreateLight(dzr_SmallFlashlightLight, "0 0 0", 0.08)  ); // Position is zero because light is attached on parent immediately.
			m_Light.AttachOnMemoryPoint(this, "beamStart", "beamEnd");
		}
	}
	
	override void OnWork( float consumed_energy )
	{
		if ( !GetGame().IsServer()  ||  !GetGame().IsMultiplayer() ) // Client side
		{
			Battery9V battery = Battery9V.Cast( GetCompEM().GetEnergySource() );
			
			if (battery  &&  m_Light)
			{
				float efficiency = battery.GetEfficiency0To1();
				
				if ( efficiency < 1 )
				{
					m_Light.SetIntensity( efficiency, GetCompEM().GetUpdateInterval() );
				}	
				
				/*else
				{
					m_Light.SetIntensity( 1, 0 );
				}*/
			}
		}
	}
	
	override void OnWorkStop()
	{
		if ( !GetGame().IsServer()  ||  !GetGame().IsMultiplayer() ) // Client side
		{
			if (m_Light)
				m_Light.FadeOut();
			
			m_Light = NULL;
		}
	}
	
	override void OnInventoryExit(Man player)
	{
		super.OnInventoryExit(player);
		
		if ( GetCompEM().IsWorking() )
		{
			if (player)
			{
				vector ori_rotate = player.GetOrientation();
				ori_rotate = ori_rotate + Vector(0,0,0);
				SetOrientation(ori_rotate);
			}
		}
	}
	
	override void SetActions()
	{
		super.SetActions();
		
		AddAction(ActionTurnOnWhileInHands);
		AddAction(ActionTurnOffWhileInHands);
	}
}