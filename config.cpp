class CfgPatches
{
	class dzr_civil_battery_items
	{
		units[] = {"dzr_SmallFlashlight","dzr_BatteryAA15V"};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"DZ_Data", "DZ_Pistols", "DZ_Weapons_Melee", "DZ_gear_Tools"};
	};
};

class CfgMods
{
	class dzr_civil_battery_items
	{
		dir = "dzr_civil_battery_items";
		picture = "";
		action = "";
		hideName = 1;
		hidePicture = 1;
		name = "DZR Civil battery items";
		credits = "DZR Lad";
		author = "DZR Lad";
		authorID = "0";
		version = "1.0";
		extra = 0;
		type = "mod";
		dependencies[] = {"World"};
		class defs
		{
			class worldScriptModule
			{
				value = "";
				files[] = {"dzr_civil_battery_items\scripts\4_World"};
			};
		};
	};
};

class CfgSlots
{
	class Slot_BatteryAA
	{
		name = "BatteryAA";
		displayName = "Батарейка AA";
		ghostIcon = "default";
	};
};

class CfgVehicles
{
	

	
	
	class Inventory_Base;
	class Flashlight: Inventory_Base //not working
	{
		attachments[]=
		{
			"BatteryAA"
		};
	};	
	
	class ItemOptics;
	class Rangefinder: ItemOptics
	{
		attachments[]=
		{
			"BatteryAA"
		};
	};	
	
	class Transmitter_Base;
	class Switchable_Base;
	class Battery9V;
	class UniversalLight;
	class LadsSmallFlashlight;
	class TLRLight;
	
	
	
	class dzr_BatteryAA15V: Battery9V //no texture! WTF?
	{
		scope = 2;
		displayName = "Батарейка АА";
		descriptionShort = "Пальчиковая батарейка 1,5 В";
		model = "\dzr_civil_battery_items\DZRBatteryAA.p3d";
		isMeleeWeapon = 1;
		weight = 3;
		absorbency = 0.8;
		itemSize[] = {1,1};
		stackedUnit = "w";
		quantityBar = 1;
		varQuantityInit = 50;
		varQuantityMin = 0.0;
		varQuantityMax = 50;
		varQuantityDestroyOnMin = 0;
		inventorySlot[] = {"BatteryAA"};
		rotationFlags = 17;
		class EnergyManager
		{
			hasIcon = 1;
			switchOnAtSpawn = 1;
			isPassiveDevice = 1;
			energyStorageMax = 50;
			energyAtSpawn = 50;
			convertEnergyToQuantity = 1;
			reduceMaxEnergyByDamageCoef = 1;
			powerSocketsCount = 1;
			compatiblePlugTypes[] = {1};
		};
		class AnimationSources
		{
			class cover
			{
				source = "user";
				animPeriod = 0.5;
				initPhase = 1;
			};
		};
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 20;
					healthLevels[] = {{1.0,{"DZ\gear\consumables\data\9v.rvmat"}},{0.7,{"DZ\gear\consumables\data\9v.rvmat"}},{0.5,{"DZ\gear\consumables\data\9v_damage.rvmat"}},{0.3,{"DZ\gear\consumables\data\9v_damage.rvmat"}},{0.0,{"DZ\gear\consumables\data\9v_destruct.rvmat"}}};
				};
			};
		};
	};
	
	class dzr_SmallFlashlight: UniversalLight
	{
		scope = 2;
		displayName = "Карманный фонарик";
		descriptionShort = "Очень компактный китайский фонарик. Не мощный, но может спасти тёмной ночью. Использует одну пальчиковую батрейку.";
		model = "\dzr_civil_battery_items\dzr_smallFlashLight.p3d";
		//model = "\LadsFlashlights\Assets\LadsElbowLight\SmallFlashlight.p3d";
		rotationFlags = 17;
		itemSize[] = {1,1};
		SingleUseActions[] = {509,510};
		animClass = "Pistol";
		hitpoints = 60;
		weight = 70;
		class EnergyManager
		{
			hasIcon = 1;
			autoSwitchOffWhenInCargo = 1;
			energyUsagePerSecond = 0.003;
			plugType = 1;
			attachmentAction = 1;
			updateInterval = 30;
			switchOnAtSpawn = 0;
			autoSwitchOff = 1;
		};
		attachments[] = {"BatteryAA"};
		repairableWithKits[] = {5,7};
		repairCosts[] = {20,15};
		//hiddenSelections[] = {"flashlight"};
		//hiddenSelectionTextures[]=
		//{
			//	"\dzr_civil_battery_items\data\dzrSmallFlashlight_co.paa",
			//	"\dzr_civil_battery_items\data\dzrSmallFlashlight_co.paa",
			//	"\dzr_civil_battery_items\data\dzrSmallFlashlight_co.paa"
		//};
		//hiddenSelectionsMaterials[] = {"LadsFlashlights\Assets\LadsElbowLight\data\SmallFlashlight.rvmat","LadsFlashlights\Assets\LadsElbowLight\data\SmallFlashlight.rvmat","LadsFlashlights\Assets\LadsElbowLight\data\SmallFlashlight.rvmat"};
	};
	
	
	
	class PersonalRadio: Transmitter_Base
	{
		attachments[] = {"BatteryAA"};
	};
	
	
	
	class Headtorch_ColorBase: Switchable_Base
	{
		attachments[] = {"BatteryAA"};
	};
	
	
};